/* jshint esversion:6 */

import Vue from 'vue';
import Vuex from 'vuex';
import createPersistedState from 'vuex-persistedstate';

import {device} from '../services/auth';
import auth from './modules/auth';
import contact from './modules/contact';
import category from './modules/category';
import announcement from './modules/announcement';
import residentreport from './modules/residentreport';
import financialreport from './modules/financialreport';
import inventory from './modules/inventory';
import movement from './modules/movement';
import region from './modules/region';
import panic from './modules/panic';
import bank from './modules/bank';
import cctv from './modules/cctv';
import product from './modules/product';
import unit from './modules/unit';
import media from './modules/media';
import info from './modules/info';
import comment from './modules/comment';
import chat from './modules/chat';
import freebies from './modules/freebies';

Vue.use(Vuex);

export default new Vuex.Store({
    plugins: [createPersistedState({
        paths: ['nid', 'auth.user', 'auth.token']
    })],
    state: {
        nid: '',
        bgColors: ['bg-red', 'bg-green', 'bg-lightgreen', 'bg-blue', 'bg-lightblue', 'bg-lightblue', 'bg-pink', 'bg-yellow', 'bg-lightyellow', 'bg-orange', 'bg-deeporange', 'bg-purple', 'bg-lightpurple', 'bg-deeppurple', 'bg-teal', 'bg-lime', 'bg-gray'],
        textColors: ['color-red', 'color-green', 'color-lightgreen', 'color-blue', 'color-lightblue', 'color-lightblue', 'color-pink', 'color-yellow', 'color-lightyellow', 'color-orange', 'color-deeporange', 'color-purple', 'color-lightpurple', 'color-deeppurple', 'color-teal', 'color-lime', 'color-gray'],
        gradientColors: ['gradient-1','gradient-2','gradient-3','gradient-4','gradient-5','gradient-6','gradient-7','gradient-8','gradient-9','gradient-10'],
    },
    getters: {
        nid: state => state.id,
        bgColors: state => state.bgColors,
        textColors: state => state.textColors,
        gradientColors: state => state.gradientColors,
    },
    mutations: {
        ADD_DEVICE: (state, nid) => {
            state.nid = nid;
        },
    },
    actions: {
        async addDevice({ commit }, payload){
            try{
                let response = await device(payload);
                let data = response.data;
                commit('ADD_DEVICE', data.nid);
                return data;
            }catch(e){}
        },
    },
    modules: {
        auth,
        contact,
        category,
        announcement,
        residentreport,
        financialreport,
        inventory,
        movement,
        region,
        panic,
        bank,
        cctv,
        product,
        unit,
        media,
        info,
        comment,
        chat,
        freebies
    }
});