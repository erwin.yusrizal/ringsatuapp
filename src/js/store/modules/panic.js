/* jshint esversion: 6 */
import { getPanics, createPanic, editPanic, viewPanic, deletePanic, createPanicReport, updatePanicInvitation } from '../../services/panic';
import { getUserBy } from '../../services/auth';

const category = {
    namespaced: true,
    state: {
        panics: [],
        panic: null,
        participant: null
    },
    getters: {
        panics: state => state.panics,
        panic: state => state.panic,
        participant: state => state.participant
    },
    mutations: {
        ADD_PANICS: (state, payload) => {
            state.panics = payload.panics;
        },
        ADD_PANIC: (state, payload) => {
            state.panics.push(payload.panic);
        },
        VIEW_PANIC: (state, payload) => {
            state.panic = payload.panic;
        },
        EDIT_PANIC: (state, payload) => {
            for(let x of state.panics){
                if(x.id == payload.id){
                    x = Object.assign(x, payload);
                    break;
                }
            }
        },                
        DELETE_PANIC: (state, payload) => {
            for(let [i, x] of state.panics){
                if(x.id == payload.id){
                    state.panics.splice(i, 1);
                    break;
                }
            }
        },
        RESET_PANICS: (state) => {
            state.panics = [];
        },
        RESET_PANIC: (state) => {
            state.panic = null;
        },
        ADD_PARTICIPANT: (state, payload) => {
            state.participant = payload.user;
        },
        DELETE_PARTICIPANT: (state, payload) => {
            state.panic = payload;
        },
        RESET_PARTICIPANT: (state) => {
            state.participant = null;
        }
    },
    actions: {
        async getPanics({ commit }, payload){
            try{
                let response = await getPanics(payload);
                commit('ADD_PANICS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createPanic({ commit }, payload){
            try{
                let response = await createPanic(payload);
                commit('ADD_PANIC', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewPanic({ commit }, payload){
            try{
                let response = await viewPanic(payload);
                commit('VIEW_PANIC', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editPanic({ commit }, payload){
            try{
                let response = await editPanic(payload);
                commit('EDIT_PANIC', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async updatePanicInvitation({ commit }, payload){
            try{
                let response = await updatePanicInvitation(payload);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async deletePanic({ commit }, payload){
            try{
                let response = await deletePanic(payload);
                commit('DELETE_PANIC', response.data.panic);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async getParticipant({ commit }, payload){
            try{
                let response = await getUserBy(payload);
                commit('ADD_PARTICIPANT', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async deleteParticipant({ commit }, payload){
            try{
                let response = await updatePanicInvitation(payload);
                commit('DELETE_PARTICIPANT', response.data.panic);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createPanicReport({ commit }, payload){
            try{
                let response = await createPanicReport(payload);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetPanics({ commit }){
            commit('RESET_PANICS');
        },
        resetParticipant({ commit }){
            commit('RESET_PARTICIPANT');
        }
    }
};

export default category;
