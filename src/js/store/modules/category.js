/* jshint esversion: 6 */
import { getCategories, getCategory } from '../../services/category';

const category = {
    namespaced: true,
    state: {
        announcementCategories: [],
        residentReportCategories: [],
        financialReportCategories: [],
        inventoryCategories: [],
        marketplaceCategories: [],
        category: null
    },
    getters: {
        announcementCategories: state => state.announcementCategories,
        residentReportCategories: state => state.residentReportCategories,
        financialReportCategories: state => state.financialReportCategories,
        inventoryCategories: state => state.inventoryCategories,
        marketplaceCategories: state => state.marketplaceCategories,
        category: state => state.category
    },
    mutations: {
        ADD_CATEGORIES: (state, payload) => {
            if(payload.module == 'announcement'){
                state.announcementCategories = payload.categories;
            }else if(payload.module == 'residentreport'){
                state.residentReportCategories = payload.categories;
            }else if(payload.module == 'financialreport'){
                state.financialReportCategories = payload.categories;
            }else if(payload.module == 'inventory'){
                state.inventoryCategories = payload.categories;
            }else if(payload.module == 'marketplace'){
                state.marketplaceCategories = payload.categories;
            }
        },
        ADD_CATEGORY: (state, payload) => {
            state.category = payload;
        },
        RESET_CATEGORIES: (state, payload) => {
            if(payload == 'announcement'){
                state.announcementCategories = [];
            }else if(payload == 'residentreport'){
                state.residentReportCategories = [];
            }else if(payload == 'financialreport'){
                state.financialReportCategories = [];
            }else if(payload == 'inventory'){
                state.inventoryCategories = [];
            }else if(payload == 'marketplace'){
                state.marketplaceCategories = [];
            }
        },
    },
    actions: {
        async getCategories({ commit }, payload){
            try{
                let response = await getCategories(payload);
                response.data.module = payload.module;
                commit('ADD_CATEGORIES', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async getCategory({ commit }, payload){
            try{
                let response = await getCategory(payload);
                commit('ADD_CATEGORY', response.data.category);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetCategories({ commit }, payload){
            commit('RESET_CATEGORIES', payload.module);
        },
    }
};

export default category;
