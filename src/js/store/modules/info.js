/* jshint esversion: 6 */
import { getInfos, viewInfo, createInfo, editInfo, deleteInfo } from '../../services/info';

const info = {
    namespaced: true,
    state: {
        slideInfos: [],
        pageInfos: [],
        info: null,
        pagination: {}
    },
    getters: {
        slideInfos: state => state.slideInfos,
        pageInfos: state => state.pageInfos,
        info: state =>  state.info,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_INFOS: (state, payload) => {
            if(payload.section == 'slide'){
                state.slideInfos = payload.infos;
            }else if(payload.section == 'page'){
                state.pageInfos = payload.infos;
                state.pagination = payload.pagination;
            }
        },
        ADD_MORE_INFOS: (state, payload) => {
            if(payload.section == 'slide'){
                state.slideInfos.push(...payload.infos);
            }else if(payload.section == 'page'){
                state.pageInfos.push(...payload.infos);
                state.pagination = payload.pagination;
            }
        },
        ADD_INFO: (state, payload) => {
            if(payload.section == 'slide'){
                state.slideInfos.push(payload.info);
            }else if(payload.section == 'page'){
                state.pageInfos.push(payload.info);
            }
        },
        VIEW_INFO: (state, payload) => {
            state.info = payload;
        },
        EDIT_INFO: (state, payload) => {
            if(payload.section == 'slide'){
                for(let x of state.slideInfos){
                    if(x.id == payload.id){
                        x = Object.assign(x, payload.info);
                        break;
                    }
                }
            }else if(payload.section == 'page'){
                for(let x of state.pageInfos){
                    if(x.id == payload.id){
                        x = Object.assign(x, payload.info);
                        break;
                    }
                }
            }            
        },
        DELETE_INFO: (state, payload) => {
            if(payload.section == 'slide'){
                for(let [i, x] of state.slideInfos){
                    if(x.id == payload.id){
                        state.slideInfos.splice(i, 1);
                        break;
                    }
                }
            }else if(payload.section == 'page'){
                for(let [i, x] of state.pageInfos){
                    if(x.id == payload.id){
                        state.pageInfos.splice(i, 1);
                        break;
                    }
                }
            }             
        },
        RESET_INFOS: (state, payload) => {
            if(payload.section == 'slide'){
                state.slideInfos = [];
            }else if(payload.section == 'page'){
                state.pageInfos = [];
            }
        },
        RESET_INFO: (state, payload) => {
            state.info = null;
        }
    },
    actions: {
        async getInfos({ commit }, payload){
            try{
                if(payload.loadmore == true){
                    let response = await getInfos(payload);
                    response.data.section = payload.section;
                    commit('ADD_MORE_INFOS', response.data);
                }else{
                    commit('RESET_INFO');
                    let response = await getInfos(payload);
                    response.data.section = payload.section;
                    commit('ADD_INFOS', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createInfo({ commit }, payload){
            try{
                let response = await createInfo(payload);
                response.data.section = payload.section;
                commit('ADD_INFO', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewInfo({ commit }, payload){
            try{
                let response = await viewInfo(payload);
                commit('VIEW_INFO', response.data.info);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editInfo({ commit }, payload){
            try{
                let response = await editInfo(payload);
                response.data.section = payload.section;
                commit('EDIT_INFO', response.data);
                return response.data;
            }catch(e){
                return e;
            }  
        },
        async deleteInfo({ commit }, payload){
            try{
                let response = await deleteInfo(payload);
                response.data.section = payload.section;
                commit('DELETE_INFO', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetInfos({ commit }, payload){
            commit('RESET_INFOS', payload);        
        },
        resetInfo({ commit }){
            commit('RESET_INFO');        
        }
    }
};

export default info;
