/* jshint esversion: 6 */
import { getRegions } from '../../services/region';

const region = {
    namespaced: true,
    state: {
        regions: []
    },
    getters: {
        regions: state => state.regions
    },
    mutations: {
        ADD_REGIONS: (state, payload) => {
            state.regions = payload.regions;
        },
        RESET_REGIONS: (state) => {
            state.regions = [];
        }
    },
    actions: {
        async getRegions({ commit }, payload){
            try{
                let response = await getRegions(payload);
                commit('ADD_REGIONS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetRegions({commit}){
            commit('RESET_REGIONS');
        }
    }
};

export default region;
