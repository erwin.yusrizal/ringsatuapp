/* jshint esversion: 6 */
import { 
    login, 
    register,
    verify, 
    reverify, 
    logout, 
    updateMe,
    updateStore, 
    getBankAccounts, 
    createBankAccount, 
    editBankAccount,
    viewBankAccount,
    deleteBankAccount,
    getUserBy,
    getNotifications
} from '../../services/auth';

const auth = {
    namespaced: true,
    state: {
        authStatus: 'login',
        user: null,
        token: null,
        verificationCode: null,
        bankaccount: null,
        singleUser: null,
        notifications: [],
        notificationPagination: {},
        total_notification: 0
    },
    getters: {
        user: state => state.user,
        token: state => state.token,
        authStatus: state => state.authStatus,
        isLoggedIn: state => !!state.token,
        verificationCode: state => state.verificationCode,
        bankaccount: state => state.bankaccount,
        singleUser: state => state.singleUser,
        notifications: state => state.notifications,
        notificationPagination: state => state.notificationPagination,
        total_notification: state => state.total_notification
    },
    mutations: {
        LOGIN_SUCCESS: (state, payload) => {
            state.authStatus = 'verify';
            state.verificationCode = payload;
        },
        REGISTER_SUCCESS: (state) => {
            state.authStatus = 'login';
        },
        VERIFY_SUCCESS: (state, payload) => {
            state.authStatus = 'loggedin';
            state.user = payload.user;
            state.token = {
                access_token: payload.access_token,
                refresh_token: payload.refresh_token
            };
            state.verificationCode = null;
        },
        REVERIFY_SUCCESS: (state, payload) => {
            state.authStatus = 'verify';
            state.verificationCode = payload;
        },
        LOGOUT_SUCCESS: (state) => {
            state.authStatus = 'login';
        },
        UPDATE_USER: (state, payload) => {
            state.user = payload;
        },
        ADD_BANK_ACCOUNT: (state, payload) => {
            state.bankaccount = payload;
        },
        SET_LOGIN: (state) => {
            state.authStatus = 'login';
        },
        SET_REGISTER: (state) => {
            state.authStatus = 'register';
        },
        ADD_SINGLE_USER: (state, payload) => {
            state.singleUser = payload;
        },
        ADD_NOTIFICATIONS: (state, payload) => {
            state.notifications = payload.notifications;
            state.notificationPagination = payload.pagination;
        },
        ADD_MORE_NOTIFICATIONS: (state, payload) => {
            state.notifications.push(...payload.notifications);
            state.notificationPagination = payload.pagination;
        },
        ADD_NOTIFICATION: (state, payload) => {
            state.notifications.push(payload);
        },
        ADD_TOTAL_NOTIFICATION: (state, payload) => {
            state.total_notification = payload;
        }
    },
    actions: {
        async doLogin({ commit }, payload){
            try{
                let response = await login(payload);
                let data = response.data;
                commit('LOGIN_SUCCESS', data.verification_code);
                return data;
            }catch(e){
                return e;
            }         
        },
        async doRegister({ commit }, payload){
            try{
                let response = await register(payload);
                let data = response.data;
                commit('REGISTER_SUCCESS');
                return data;
            }catch(e){
                return e;
            }         
        },
        async doVerify({ commit }, payload){
            try{
                let response = await verify(payload);
                let data = response.data;
                commit('VERIFY_SUCCESS', data);
                return data;
            }catch(e){
                return e;
            }         
        },
        async doReverify({ commit }, payload){
            try{
                let response = await reverify(payload);
                let data = response.data;
                commit('REVERIFY_SUCCESS', data.verification_code);
                return data;
            }catch(e){
                return e;
            }         
        },
        async doLogout({ dispatch }){
            try{
                let response = await logout();
                return response;
            }catch(e){
                // eslint-disable-next-line no-console
                console.log(e);
            }
        },
        async updateStore({ commit }, payload){
            try{
                let response = await updateStore(payload);
                let data = response.data;
                commit('UPDATE_USER', data.user);
                return data;
            }catch(e){
                return e;
            }  
        },
        async getBankAccounts({ commit }, payload){
            try{
                let response = await getBankAccounts(payload);
                let data = response.data;
                commit('UPDATE_USER', data.user);
                return data;
            }catch(e){
                return e;
            }  
        },
        async createBankAccount({ commit }, payload){
            try{
                let response = await createBankAccount(payload);
                let data = response.data;
                commit('UPDATE_USER', data.user);
                return data;
            }catch(e){
                return e;
            }  
        },
        async viewBankAccount({ commit }, payload){
            try{
                let response = await viewBankAccount(payload);
                let data = response.data;
                commit('ADD_BANK_ACCOUNT', data.bankaccount);
                return data;
            }catch(e){
                return e;
            }  
        },
        async editBankAccount({ commit }, payload){
            try{
                let response = await editBankAccount(payload);
                let data = response.data;
                commit('UPDATE_USER', data.user);
                return data;
            }catch(e){
                return e;
            }  
        },
        async deleteBankAccount({ commit }, payload){
            try{
                let response = await deleteBankAccount(payload);
                let data = response.data;
                commit('UPDATE_USER', data.user);
                return data;
            }catch(e){
                return e;
            }  
        },
        async updateProfile({ commit }, payload){
            try{
                commit('UPDATE_USER', payload.user);
            }catch(e){
                return e;
            }  
        },
        async updateMe({ commit }, payload){
            try{
                let response = await updateMe(payload);
                let data = response.data;
                commit('UPDATE_USER', data.me);
                return data;
            }catch(e){
                return e;
            } 
        },
        async getNotifications({ commit }, payload){
            try{
                let response = await getNotifications(payload);
                let data = response.data;
                if(payload.hasOwnProperty('total_only') && payload.total_only == true){
                    commit('ADD_TOTAL_NOTIFICATION', data.total_notification);
                }else if(payload.hasOwnProperty('loadmore') && payload.loadmore == true){
                    commit('ADD_MORE_NOTIFICATIONS', data);
                }else{
                    commit('ADD_NOTIFICATIONS', data);
                }
                
                return data;
            }catch(e){
                return e;
            } 
        },
        async getUserBy({ commit }, payload){
            try{
                let response = await getUserBy(payload);
                let data = response.data;
                commit('ADD_SINGLE_USER', data.user);
                return data;
            }catch(e){
                return e;
            } 
        },
        async setLogin({ commit }){
            try{
                commit('SET_LOGIN');
            }catch(e){
                return e;
            }  
        },
        async setRegister({ commit }){
            try{
                commit('SET_REGISTER');
            }catch(e){
                return e;
            }  
        }
    }
};

export default auth;
