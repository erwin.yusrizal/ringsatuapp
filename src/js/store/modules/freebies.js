/* jshint esversion: 6 */
import { getFreebies, viewFreebie, createFreebie, editFreebie, deleteFreebie } from '../../services/freebies';

const freebies = {
    namespaced: true,
    state: {
        freebies: [],
        pagination: {},
        freebie: null
    },
    getters: {
        freebies: state =>  state.freebies,
        pagination: state => state.pagination,
        freebie: state =>  state.freebie,
    },
    mutations: {
        ADD_FREEBIES: (state, payload) => {
            state.freebies = payload.freebies;
            state.pagination = payload.pagination;
        },
        ADD_MORE_FREEBIES: (state, payload) => {
            state.freebies.push(...payload.freebies);
            state.pagination = payload.pagination;
        },
        ADD_FREEBIE: (state, payload) => {
            state.freebies.push(payload.freebie);
        },
        VIEW_FREEBIE: (state, payload) => {
            state.freebie = payload;
        },
        EDIT_FREEBIE: (state, payload) => {
            for(let x of state.freebies){
                if(x.id == payload.id){
                    x = Object.assign(x, payload.freebie);
                    break;
                }
            }            
        },
        DELETE_FREEBIE: (state, payload) => {
            for(let [i, x] of state.freebies){
                if(x.id == payload.id){
                    state.freebies.splice(i, 1);
                    break;
                }
            }            
        },
        RESET_FREEBIES: (state, payload) => {
            state.freebies = [];
        },
        RESET_FREEBIE: (state, payload) => {
            state.freebie = null;
        }
    },
    actions: {
        async getFreebies({ commit }, payload){
            try{
                let response = await getFreebies(payload);
                if(payload.loadmore == true){                    
                    commit('ADD_MORE_FREEBIES', response.data);
                }else{
                    commit('RESET_FREEBIE');
                    commit('ADD_FREEBIES', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createFreebie({ commit }, payload){
            try{
                let response = await createFreebie(payload);
                commit('ADD_FREEBIE', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewFreebie({ commit }, payload){
            try{
                let response = await viewFreebie(payload);
                commit('VIEW_FREEBIE', response.data.freebie);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editFreebie({ commit }, payload){
            try{
                let response = await editFreebie(payload);
                commit('EDIT_FREEBIE', response.data);
                return response.data;
            }catch(e){
                return e;
            }  
        },
        async deleteFreebie({ commit }, payload){
            try{
                let response = await deleteFreebie(payload);
                commit('DELETE_FREEBIE', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetFreebies({ commit }, payload){
            commit('RESET_FREEBIES', payload);        
        },
        resetFreebie({ commit }){
            commit('RESET_FREEBIE');        
        }
    }
};

export default freebies;
