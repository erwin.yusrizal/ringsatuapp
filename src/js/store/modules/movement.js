/* jshint esversion: 6 */
import { getMovements, createMovement, viewMovement, editMovement } from '../../services/movement';

const movement = {
    namespaced: true,
    state: {
        movements: [],
        movement: {},
        pagination: {}
    },
    getters: {
        movements: state => state.movements,
        movement: state => state.movement,
        pagination: state => state.pagination
    },
    mutations: {
        RESET_MOVEMENTS: (state) => {
            state.movements = [];
        },
        RESET_MOVEMENT: (state) => {
            state.movement = [];
        },
        ADD_MOVEMENTS: (state, payload) => {
            state.movements = payload.movements;
            state.pagination = payload.pagination;
        },
        ADD_MOVEMENT: (state, movement) => {
            state.movements.push(movement);
        },
        VIEW_MOVEMENT: (state, movement) => {
            state.movement = movement;
        },
        EDIT_MOVEMENT: (state, movement) => {
            for(let x of state.movements){
                if(x.id == movement.id){
                    x = Object.assign(x, movement);
                    break;
                }
            }
        }
    },
    actions: {
        async getMovements({ commit }, payload){
            try{
                commit('RESET_MOVEMENTS');
                commit('RESET_MOVEMENT');
                let response = await getMovements(payload);
                commit('ADD_MOVEMENTS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createMovement({ commit }, payload){
            try{
                let response = await createMovement(payload);
                commit('ADD_MOVEMENT', response.data.movement);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewMovement({ commit }, payload){
            try{
                let response = await viewMovement(payload);
                commit('VIEW_MOVEMENT', response.data.movement);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editMovement({ commit }, payload){
            try{
                let response = await editMovement(payload);
                commit('ADD_MOVEMENT', response.data.movement);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetMovement({ commit }){
            commit('RESET_MOVEMENT');       
        }
    }
};

export default movement;
