/* jshint esversion: 6 */
import { getAnnouncements, createAnnouncement, viewAnnouncement, editAnnouncement } from '../../services/announcement';

const announcement = {
    namespaced: true,
    state: {
        announcements: [],
        announcement: null,
        pagination: {}
    },
    getters: {
        announcements: state => state.announcements,
        announcement: state => state.announcement,
        pagination: state => state.pagination
    },
    mutations: {
        RESET_ANNOUNCEMENTS: (state) => {
            state.announcements = [];
        },
        RESET_ANNOUNCEMENT: (state) => {
            state.announcement = null;
        },
        ADD_ANNOUNCEMENTS: (state, payload) => {
            state.announcements = payload.announcements;
            state.pagination = payload.pagination;
        },
        ADD_MORE_ANNOUNCEMENTS: (state, payload) => {
            state.announcements.push(...payload.announcements);
            state.pagination = payload.pagination;
        },
        ADD_ANNOUNCEMENT: (state, announcement) => {
            state.announcement = announcement;
        }
    },
    actions: {
        async getAnnouncements({ commit }, payload){
            try{
                if(payload.loadmore == true){
                    let response = await getAnnouncements(payload);
                    commit('ADD_MORE_ANNOUNCEMENTS', response.data);
                }else{
                    commit('RESET_ANNOUNCEMENTS');
                    commit('RESET_ANNOUNCEMENT');
                    let response = await getAnnouncements(payload);
                    commit('ADD_ANNOUNCEMENTS', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewAnnouncement({ commit }, payload){
            try{
                let response = await viewAnnouncement(payload);
                commit('ADD_ANNOUNCEMENT', response.data.announcement);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetAnnouncements({ commit }){
            commit('RESET_ANNOUNCEMENTS');       
        },
        resetAnnouncement({ commit }){
            commit('RESET_ANNOUNCEMENT');       
        }
    }
};

export default announcement;
