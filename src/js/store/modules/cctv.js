/* jshint esversion: 6 */
import { getCCTVS, viewCCTV } from '../../services/cctv';

const cctv = {
    namespaced: true,
    state: {
        cctvs: [],
        cctv: null
    },
    getters: {
        cctvs: state => state.cctvs,
        cctv: state => state.cctv
    },
    mutations: {
        ADD_CCTVS: (state, payload) => {
            state.cctvs = payload.cctvs;
        },
        ADD_CCTV: (state, payload) => {
            state.cctv = payload;
        }
    },
    actions: {
        async getCCTVS({ commit }, payload){
            try{
                let response = await getCCTVS(payload);
                commit('ADD_CCTVS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewCCTV({ commit }, payload){
            try{
                let response = await viewCCTV(payload);
                commit('ADD_CCTV', response.data.cctv);
                return response.data;
            }catch(e){
                return e;
            }         
        }
    }
};

export default cctv;
