/* jshint esversion: 6 */
import { shuffle } from '../../utils';
import { getProducts, createProduct, viewProduct, editProduct, deleteProduct } from '../../services/product';

const product = {
    namespaced: true,
    state: {
        homeProducts: [],
        marketplaceProducts: [],
        myProducts: [],
        pagination: {},
        product: null,
        allmedias: []
    },
    getters: {
        homeProducts: state => state.homeProducts,
        marketplaceProducts: state => state.marketplaceProducts,
        myProducts: state => state.myProducts,
        pagination: state => state.pagination,
        product: state => state.product,
        allmedias: state => state.allmedias
    },
    mutations: {
        RESET_PRODUCTS: (state, payload) => {
            if(payload == 'home'){
                state.homeProducts = [];
            }else if(payload == 'marketplace'){
                state.marketplaceProducts = [];
            }else if(payload == 'my'){
                state.myProducts = [];
            }            
        },
        RESET_PRODUCT: (state) => {
            state.product = null;
        },
        RESET_ALLMEDIAS: (state) => {
            state.allmedias = [];
        },
        ADD_PRODUCTS: (state, payload) => {
            state.allmedias = [];
            if(payload.pageName == 'home'){
                state.homeProducts = payload.products;
                state.pagination = payload.pagination;

                for(const x of payload.products){
                    if(x.medias.length > 0){
                        state.allmedias.push(x.medias[0]);
                    }
                }
            }else if(payload.pageName == 'marketplace'){
                state.marketplaceProducts = payload.products;
                state.pagination = payload.pagination;

                for(const x of payload.products){
                    if(x.medias.length > 0){
                        state.allmedias.push(x.medias[0]);
                    }
                }
            }else if(payload.pageName == 'my'){
                state.myProducts = payload.products;
                state.pagination = payload.pagination;

                for(const x of payload.products){
                    if(x.medias.length > 0){
                        state.allmedias.push(x.medias[0]);
                    }
                }
            }            
        },
        ADD_MORE_PRODUCTS: (state, payload) => {
            if(payload.pageName == 'home'){
                state.homeProducts.push(...payload.products);
                state.pagination = payload.pagination;

                for(const x of payload.products){
                    if(x.medias.length > 0){
                        state.allmedias.push(x.medias[0]);
                    }
                }
            }else if(payload.pageName == 'marketplace'){
                state.marketplaceProducts.push(...payload.products);
                state.pagination = payload.pagination;

                for(const x of payload.products){
                    if(x.medias.length > 0){
                        state.allmedias.push(x.medias[0]);
                    }
                }
            }else if(payload.pageName == 'my'){
                state.myProducts.push(...payload.products);
                state.pagination = payload.pagination;

                for(const x of payload.products){
                    if(x.medias.length > 0){
                        state.allmedias.push(x.medias[0]);
                    }
                }
            }            
        },
        ADD_PRODUCT: (state, payload) => {
            if(payload.pageName == 'home'){
                state.homeProducts.push(payload.product);
            }else if(payload.pageName == 'marketplace'){
                state.marketplaceProducts.push(payload.product);
            }else if(payload.pageName == 'my'){
                state.myProducts.push(payload.product);
            }            
        },
        VIEW_PRODUCT: (state, payload) => {
            state.product = payload;
        },
        EDIT_PRODUCT: (state, payload) => {
            if(payload.pageName == 'home'){
                for(let x of state.homeProducts){
                    if(x.id == payload.id){
                        x = Object.assign(x, payload);
                        break;
                    }
                }
            }else if(payload.pageName == 'marketplace'){
                for(let x of state.marketplaceProducts){
                    if(x.id == payload.id){
                        x = Object.assign(x, payload);
                        break;
                    }
                }
            }else if(payload.pageName == 'my'){
                for(let x of state.myProducts){
                    if(x.id == payload.id){
                        x = Object.assign(x, payload);
                        break;
                    }
                }
            }            
        },
        DELETE_PRODUCT: (state, payload) => {
            if(payload.pageName == 'home'){
                for(let [i, x] of state.homeProducts){
                    if(x.id == payload.id){
                        state.homeProducts.splice(i, 1);
                        break;
                    }
                }
            }else if(payload.pageName == 'marketplace'){
                for(let [i, x] of state.marketplaceProducts){
                    if(x.id == payload.id){
                        state.marketplaceProducts.splice(i, 1);
                        break;
                    }
                }
            }else if(payload.pageName == 'my'){
                for(let [i, x] of state.myProducts){
                    if(x.id == payload.id){
                        state.myProducts.splice(i, 1);
                        break;
                    }
                }
            }             
        }
    },
    actions: {
        async getProducts({ commit }, payload){
            try{
                if(payload.loadmore == true){                    
                    let response = await getProducts(payload);
                    response.data.pageName = payload.pageName;
                    commit('ADD_MORE_PRODUCTS', response.data);
                }else{
                    commit('RESET_PRODUCTS', payload.pageName);
                    commit('RESET_ALLMEDIAS');
                    commit('RESET_PRODUCT');
                    let response = await getProducts(payload);
                    response.data.pageName = payload.pageName;
                    commit('ADD_PRODUCTS', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createProduct({ commit }, payload){
            try{
                commit('RESET_ALLMEDIAS');
                commit('RESET_PRODUCT');
                let response = await createProduct(payload);
                response.data.pageName = payload.pageName;
                commit('ADD_PRODUCT', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewProduct({ commit }, payload){
            try{
                commit('RESET_ALLMEDIAS');
                commit('RESET_PRODUCT');
                let response = await viewProduct(payload);
                commit('VIEW_PRODUCT', response.data.product);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editProduct({ commit }, payload){
            try{
                commit('RESET_ALLMEDIAS');
                commit('RESET_PRODUCT');
                let response = await editProduct(payload);
                response.data.pageName = payload.pageName;
                commit('EDIT_PRODUCT', response.data.product);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async deleteProduct({ commit }, payload){
            try{
                let response = await deleteProduct(payload);
                response.data.pageName = payload.pageName;
                commit('DELETE_PRODUCT', response.data.product);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetProducts({ commit }, payload){
            commit('RESET_PRODUCTS');       
        },
        resetProduct({ commit }){
            commit('RESET_PRODUCT');       
        },
    }
};

export default product;
