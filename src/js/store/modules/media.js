/* jshint esversion: 6 */

import { deleteMedia, deleteTmpMedia } from '../../services/media';

const media = {
    namespaced: true,
    state: {},
    getters: {},
    mutations: {},
    actions: {        
        async deleteMedia({ commit }, payload){
            try{
                let response = await deleteMedia(payload);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async deleteTmpMedia({ commit }, payload){
            try{
                let response = await deleteTmpMedia(payload);
                return response.data;
            }catch(e){
                return e;
            }         
        }
    }
};

export default media;
