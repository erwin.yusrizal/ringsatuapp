/* jshint esversion: 6 */
import { getFinancialReports, createFinancialReport, viewFinancialReport, editFinancialReport, deleteFinancialReport } from '../../services/financialreport';

const financialreport = {
    namespaced: true,
    state: {
        financialreports: [],
        financialreport: {},
        pagination: {},
        totaldebit: 0,
        totalcredit: 0
    },
    getters: {
        financialreports: state => state.financialreports,
        financialreport: state => state.financialreport,
        pagination: state => state.pagination,
        totaldebit: state => state.totaldebit,
        totalcredit: state => state.totalcredit
    },
    mutations: {
        RESET_FINANCIAL_REPORTS: (state) => {
            state.financialreports = [];
        },
        RESET_FINANCIAL_REPORT: (state) => {
            state.financialreport = [];
        },
        ADD_FINANCIAL_REPORTS: (state, payload) => {
            state.financialreports = payload.financialreports;
            state.pagination = payload.pagination;
            state.totalcredit = payload.total_credit;
            state.totaldebit = payload.total_debit;
        },
        ADD_MORE_FINANCIAL_REPORTS: (state, payload) => {
            state.financialreports.push(...payload.financialreports);
            state.pagination = payload.pagination;
            state.totalcredit = payload.total_credit;
            state.totaldebit = payload.total_debit;
        },
        ADD_FINANCIAL_REPORT: (state, payload) => {
            state.financialreports.push(payload);
        },
        VIEW_FINANCIAL_REPORT: (state, payload) => {
            state.financialreport = payload;
        },
        EDIT_FINANCIAL_REPORT: (state, payload) => {
            for(let x of state.financialreports){
                if(x.id == payload.id){
                    x = Object.assign(x, payload);
                    break;
                }
            }
        },
        DELETE_FINANCIAL_REPORT: (state, payload) => {
            for(let [i, x] of state.financialreports){
                if(x.id == payload.id){
                    state.financialreports.splice(i, 1);
                    break;
                }
            }           
        }
    },
    actions: {
        async getFinancialReports({ commit }, payload){
            try{
                if(payload.loadmore == true){
                    let response = await getFinancialReports(payload);
                    commit('ADD_MORE_FINANCIAL_REPORTS', response.data);
                }else{
                    commit('RESET_FINANCIAL_REPORTS');
                    commit('RESET_FINANCIAL_REPORT');
                    let response = await getFinancialReports(payload);
                    commit('ADD_FINANCIAL_REPORTS', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createFinancialReport({ commit }, payload){
            try{
                let response = await createFinancialReport(payload);
                commit('ADD_FINANCIAL_REPORT', response.data.financialreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewFinancialReport({ commit }, payload){
            try{
                let response = await viewFinancialReport(payload);
                commit('VIEW_FINANCIAL_REPORT', response.data.financialreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editFinancialReport({ commit }, payload){
            try{
                let response = await editFinancialReport(payload);
                commit('ADD_FINANCIAL_REPORT', response.data.financialreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async deleteFinancialReport({ commit }, payload){
            try{
                let response = await deleteFinancialReport(payload);
                commit('DELETE_FINANCIAL_REPORT', response.data.financialreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetFinancialReports({ commit }){
            commit('RESET_FINANCIAL_REPORTS');      
        },
        resetFinancialReport({ commit }){
            commit('RESET_FINANCIAL_REPORT');      
        }
    }
};

export default financialreport;
