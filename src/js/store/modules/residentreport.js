/* jshint esversion: 6 */
import { getResidentReports, createResidentReport, viewResidentReport, editResidentReport, deleteResidentReport } from '../../services/residentreport';

const residentreport = {
    namespaced: true,
    state: {
        residentreports: [],
        residentreport: null,
        pagination: {}
    },
    getters: {
        residentreports: state => state.residentreports,
        residentreport: state => state.residentreport,
        pagination: state => state.pagination
    },
    mutations: {
        RESET_RESIDENT_REPORTS: (state) => {
            state.residentreports = [];
        },
        RESET_RESIDENT_REPORT: (state) => {
            state.residentreport = null;
        },
        ADD_RESIDENT_REPORTS: (state, payload) => {
            state.residentreports = payload.residentreports;
            state.pagination = payload.pagination;            
        },
        ADD_MORE_RESIDENT_REPORTS: (state, payload) => {
            state.residentreports.push(...payload.residentreports);
            state.pagination = payload.pagination;            
        },
        ADD_RESIDENT_REPORT: (state, payload) => {
            state.residentreports.push(payload);
        },
        VIEW_RESIDENT_REPORT: (state, payload) => {
            state.residentreport = payload;
        },
        EDIT_RESIDENT_REPORT: (state, payload) => {
            for(let x of state.residentreports){
                if(x.id == payload.id){
                    x = Object.assign(x, payload);
                    break;
                }
            }
        },
        DELETE_RESIDENT_REPORT: (state, payload) => {
            for(let [i, x] of state.residentreports){
                if(x.id == payload.id){
                    state.residentreports.splice(i, 1);
                    break;
                }
            }           
        }
    },
    actions: {
        async getResidentReports({ commit }, payload){
            try{
                if(payload.loadmore == true){
                    let response = await getResidentReports(payload);
                    commit('ADD_MORE_RESIDENT_REPORTS', response.data);
                }else{
                    commit('RESET_RESIDENT_REPORTS');
                    commit('RESET_RESIDENT_REPORT');
                    let response = await getResidentReports(payload);
                    commit('ADD_RESIDENT_REPORTS', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createResidentReport({ commit }, payload){
            try{
                let response = await createResidentReport(payload);
                commit('ADD_RESIDENT_REPORT', response.data.residentreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewResidentReport({ commit }, payload){
            try{
                let response = await viewResidentReport(payload);
                commit('VIEW_RESIDENT_REPORT', response.data.residentreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editResidentReport({ commit }, payload){
            try{
                let response = await editResidentReport(payload);
                commit('ADD_RESIDENT_REPORT', response.data.residentreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async deleteResidentReport({ commit }, payload){
            try{
                let response = await deleteResidentReport(payload);
                commit('DELETE_RESIDENT_REPORT', response.data.residentreport);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetResidentReports({ commit }){
            commit('RESET_RESIDENT_REPORTS');      
        },
        resetResidentReport({ commit }){
            commit('RESET_RESIDENT_REPORT');      
        }
    }
};

export default residentreport;
