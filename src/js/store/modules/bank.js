/* jshint esversion: 6 */
import { getBanks } from '../../services/bank';

const bank = {
    namespaced: true,
    state: {
        banks: [],
        pagination: {}
    },
    getters: {
        banks: state => state.banks,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_BANKS: (state, payload) => {
            state.banks = payload.banks;
            state.pagination = payload.pagination;
        }
    },
    actions: {
        async getBanks({ commit }, payload){
            try{
                let response = await getBanks(payload);
                commit('ADD_BANKS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        }
    }
};

export default bank;
