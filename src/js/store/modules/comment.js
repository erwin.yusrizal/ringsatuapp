/* jshint esversion: 6 */
import { getComments, viewComment, createComment, editComment, deleteComment } from '../../services/comment';

const comment = {
    namespaced: true,
    state: {
        comments: [],
        comment: null,
        pagination: {}
    },
    getters: {
        comments: state => state.comments,
        comment: state =>  state.comment,
        pagination: state => state.pagination
    },
    mutations: {
        ADD_COMMENTS: (state, payload) => {
            state.comments = payload.comments;
            state.pagination = payload.pagination;
        },
        ADD_MORE_COMMENTS: (state, payload) => {
            state.comments.unshift(...payload.comments);
            state.pagination = payload.pagination;
        },
        ADD_COMMENT: (state, payload) => {
            state.comments.push(payload);
        },
        VIEW_COMMENT: (state, payload) => {
            state.comment = payload;
        },
        EDIT_COMMENT: (state, payload) => {
            for(let x of state.comments){
                if(x.id == payload.id){
                    x = Object.assign(x, payload);
                    break;
                }
            }
        },
        DELETE_COMMENT: (state, payload) => {
            for(let [i, x] of state.comment){
                if(x.id == payload.id){
                    state.comment.splice(i, 1);
                    break;
                }
            }
        }
    },
    actions: {
        async getComments({ commit }, payload){
            try{
                let response = await getComments(payload);
                if(payload.loadmore == true){
                    commit('ADD_MORE_COMMENTS', response.data);
                }else{
                    commit('ADD_COMMENTS', response.data);
                }                
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createComment({ commit }, payload){
            try{
                let response = await createComment(payload);
                commit('ADD_COMMENT', response.data.comment);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewComment({ commit }, payload){
            try{
                let response = await viewComment(payload);
                commit('VIEW_COMMENT', response.data.comment);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editComment({ commit }, payload){
            try{
                let response = await editComment(payload);
                let data = response.data;
                commit('EDIT_COMMENT', data.comment);
                return data;
            }catch(e){
                return e;
            }  
        },
        async deleteComment({ commit }, payload){
            try{
                let response = await deleteComment(payload);
                commit('DELETE_COMMENT', response.data.comment);
                return response.data;
            }catch(e){
                return e;
            }         
        }
    }
};

export default comment;
