/* jshint esversion: 6 */
import { getContacts } from '../../services/contact';

const contact = {
    namespaced: true,
    state: {
        contacts: []
    },
    getters: {
        contacts: state => state.contacts
    },
    mutations: {
        ADD_CONTACTS: (state, payload) => {
            state.contacts = payload.contacts;
        }
    },
    actions: {
        async getContacts({ commit }, payload){
            try{
                let response = await getContacts(payload);
                commit('ADD_CONTACTS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        }
    }
};

export default contact;
