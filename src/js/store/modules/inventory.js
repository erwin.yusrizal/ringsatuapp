/* jshint esversion: 6 */
import { getInventories, createInventory, viewInventory, editInventory } from '../../services/inventory';

const inventory = {
    namespaced: true,
    state: {
        inventories: [],
        inventory: null,
        pagination: {}
    },
    getters: {
        inventories: state => state.inventories,
        inventory: state => state.inventory,
        pagination: state => state.pagination
    },
    mutations: {
        RESET_INVENTORIES: (state) => {
            state.inventories = [];
        },
        RESET_INVENTORY: (state) => {
            state.inventory = null;
        },
        ADD_INVENTORIES: (state, payload) => {
            state.inventories = payload.inventories;
            state.pagination = payload.pagination;
        },
        ADD_MORE_INVENTORIES: (state, payload) => {
            state.inventories.push(...payload.inventories);
            state.pagination = payload.pagination;
        },
        ADD_INVENTORY: (state, inventory) => {
            state.inventories.push(inventory);
        },
        VIEW_INVENTORY: (state, inventory) => {
            state.inventory = inventory;
        },
        EDIT_INVENTORY: (state, inventory) => {
            for(let x of state.inventories){
                if(x.id == inventory.id){
                    x = Object.assign(x, inventory);
                    break;
                }
            }
        }
    },
    actions: {
        async getInventories({ commit }, payload){
            try{
                if(payload.loadmore == true){
                    let response = await getInventories(payload);
                    commit('ADD_INVENTORIES', response.data);
                }else{
                    commit('RESET_INVENTORIES');
                    commit('RESET_INVENTORY');
                    let response = await getInventories(payload);
                    commit('ADD_INVENTORIES', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createInventory({ commit }, payload){
            try{
                let response = await createInventory(payload);
                commit('ADD_INVENTORY', response.data.inventory);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewInventory({ commit }, payload){
            try{
                let response = await viewInventory(payload);
                commit('VIEW_INVENTORY', response.data.inventory);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async editInventory({ commit }, payload){
            try{
                let response = await editInventory(payload);
                commit('ADD_INVENTORY', response.data.inventory);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        resetInventories({ commit }){
            commit('RESET_INVENTORIES');       
        },
        resetInventory({ commit }){
            commit('RESET_INVENTORY');       
        }
    }
};

export default inventory;
