/* jshint esversion: 6 */
import { getUnits } from '../../services/unit';

const unit = {
    namespaced: true,
    state: {
        units: []
    },
    getters: {
        units: state => state.units
    },
    mutations: {
        ADD_UNITS: (state, payload) => {
            state.units = payload.units;
        }
    },
    actions: {
        async getUnits({ commit }, payload){
            try{
                let response = await getUnits(payload);
                commit('ADD_UNITS', response.data);
                return response.data;
            }catch(e){
                return e;
            }         
        }
    }
};

export default unit;
