/* jshint esversion: 6 */
import { getChats, createChat, viewChatMessage, createChatMessage } from '../../services/chat';

const chat = {
    namespaced: true,
    state: {
        chats: [],
        chat: null,
        messages: [],
        pagination: {},
        totalSender: 0,
        totalRecipient: 0
    },
    getters: {
        chats: state => state.chats,
        chat: state => state.chat,
        messages: state =>  state.messages,
        pagination: state => state.pagination,
        totalSender: state => state.totalSender,
        totalRecipient: state => state.totalRecipient
    },
    mutations: {
        ADD_CHATS: (state, payload) => {
            state.chats = payload.chats;
            state.pagination = payload.pagination;
        },
        ADD_MORE_CHATS: (state, payload) => {
            state.chats.push(payload.chats);
            state.pagination = payload.pagination;
        },
        ADD_MESSAGES: (state, payload) => {
            state.chat = payload.chat;
            state.messages = payload.messages;
            state.pagination = payload.pagination;
        },
        ADD_MORE_MESSAGES: (state, payload) => {
            state.messages.unshift(...payload.messages);
            state.pagination = payload.pagination;
        },
        ADD_CHAT: (state, payload) => {
            state.chat = payload;
            state.chats.push(payload);
        },
        ADD_MESSAGE: (state, payload) => {
            state.messages.push(payload);
        },
        ADD_TOTAL: (state, payload) => {
            state.totalSender = payload.total_sender;
            state.totalRecipient = payload.total_recipient;
        },
        RESET_CHATS: (state) => {
            state.chats = [];
        }
    },
    actions: {
        async getChats({ commit }, payload){
            try{
                let response = await getChats(payload);
                if(payload.hasOwnProperty('totalonly')){
                    commit('ADD_TOTAL', response.data);
                }else{
                    if(payload.loadmore == true){
                        commit('ADD_MORE_CHATS', response.data);
                    }else{
                        commit('ADD_CHATS', response.data);
                    }
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createChat({ commit }, payload){
            try{
                let response = await createChat(payload);
                commit('ADD_CHAT', response.data.chat);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async createChatMessage({ commit }, payload){
            try{
                let response = await createChatMessage(payload);
                commit('ADD_MESSAGE', response.data.message);
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async viewChatMessage({ commit }, payload){
            try{
                let response = await viewChatMessage(payload);
                if(payload.loadmore == true){
                    commit('ADD_MORE_MESSAGES', response.data);
                }else{
                    commit('ADD_MESSAGES', response.data);
                }
                return response.data;
            }catch(e){
                return e;
            }         
        },
        async addChatMessage({ commit }, payload){
            try{
                commit('ADD_MESSAGE', payload);
            }catch(e){
                return e;
            }         
        },
        resetChats({ commit }){
            commit('RESET_CHATS');
        }
    }
};

export default chat;
