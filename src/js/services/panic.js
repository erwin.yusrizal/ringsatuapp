/* jshint esversion: 6 */

import Vue from 'vue';

export const getPanics = (payload) => {
    return Vue.http.get('/panics', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createPanic = (payload) => {
    return Vue.http.post('/panics', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editPanic = (payload) => {
    return Vue.http.put('/panics/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewPanic = (payload) => {
    return Vue.http.get('/panics/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deletePanic= (payload) => {
    return Vue.http.delete('/panics/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createPanicReport = (payload) => {
    return Vue.http.post('/panic/reports', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const updatePanicInvitation = (payload) => {
    return Vue.http.put('/panics/'+payload.id+'/invitation', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};