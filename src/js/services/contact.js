/* jshint esversion: 6 */

import Vue from 'vue';

export const getContacts = (payload) => {
    return Vue.http.get('/contacts', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};