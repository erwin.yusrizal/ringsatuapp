/* jshint esversion: 6 */

import Vue from 'vue';

export const getCCTVS = (payload) => {
    return Vue.http.get('/cctv', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewCCTV = (payload) => {
    return Vue.http.get('/cctv/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};