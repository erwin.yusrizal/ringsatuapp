/* jshint esversion: 6 */

import Vue from 'vue';

export const getProducts = (payload) => {
    return Vue.http.get('/products', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createProduct = (payload) => {
    return Vue.http.post('/products', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewProduct = (payload) => {
    return Vue.http.get('/products/'+payload.id, {params:payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editProduct = (payload) => {
    return Vue.http.put('/products/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteProduct = (payload) => {
    return Vue.http.delete('/products/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};