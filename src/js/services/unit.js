/* jshint esversion: 6 */

import Vue from 'vue';

export const getUnits = (payload) => {
    return Vue.http.get('/units', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};