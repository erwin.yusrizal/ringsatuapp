/* jshint esversion: 6 */

import Vue from 'vue';

export const getFreebies = (payload) => {
    return Vue.http.get('/freebies', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createFreebie = (payload) => {
    return Vue.http.post('/freebies', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewFreebie = (payload) => {
    return Vue.http.get('/freebies/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editFreebie = (payload) => {
    return Vue.http.put('/freebies/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteFreebie = (payload) => {
    return Vue.http.delete('/freebies/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};