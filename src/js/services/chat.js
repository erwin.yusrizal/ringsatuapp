/* jshint esversion: 6 */

import Vue from 'vue';

export const getChats = (payload) => {
    return Vue.http.get('/chats', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createChat = (payload) => {
    return Vue.http.post('/chats', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewChatMessage = (payload) => {
    return Vue.http.get('/chats/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createChatMessage = (payload) => {
    return Vue.http.post('/chats/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};
