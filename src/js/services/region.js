/* jshint esversion: 6 */

import Vue from 'vue';

export const getRegions = (payload) => {
    return Vue.http.get('/regions', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};