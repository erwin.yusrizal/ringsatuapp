/* jshint esversion: 6 */

import Vue from 'vue';
import store from '../store';

export const getMovements = (payload) => {
    return Vue.http.get('/inventory/movements', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createMovement = (payload) => {
    return Vue.http.post('/inventory/movements', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewMovement = (payload) => {
    return Vue.http.get('/inventory/movements/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editMovement = (payload) => {
    return Vue.http.put('/inventory/movements/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};