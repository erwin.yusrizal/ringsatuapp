/* jshint esversion: 6 */

import Vue from 'vue';

export const getOrders = (payload) => {
    return Vue.http.get('/orders', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createOrder = (payload) => {
    return Vue.http.post('/orders', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewOrder = (payload) => {
    return Vue.http.get('/orders/'+payload.id, {params:payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editOrder = (payload) => {
    return Vue.http.put('/orders/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteOrder = (payload) => {
    return Vue.http.delete('/orders/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};