/* jshint esversion: 6 */

import Vue from 'vue';
import store from '../store';

export const getInventories = (payload) => {
    return Vue.http.get('/inventory', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createInventory = (payload) => {
    return Vue.http.post('/inventory', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewInventory = (payload) => {
    return Vue.http.get('/inventory/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editInventory = (payload) => {
    return Vue.http.put('/inventory/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};