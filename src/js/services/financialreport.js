/* jshint esversion: 6 */

import Vue from 'vue';
import store from '../store';

export const getFinancialReports = (payload) => {
    return Vue.http.get('/financialreports', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createFinancialReport = (payload) => {
    return Vue.http.post('/financialreports', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewFinancialReport = (payload) => {
    return Vue.http.get('/financialreports/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editFinancialReport = (payload) => {
    return Vue.http.put('/financialreports/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteFinancialReport = (payload) => {
    return Vue.http.delete('/financialreports/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};