/* jshint esversion: 6 */

import Vue from 'vue';
import store from '../store';

export const getAnnouncements = (payload) => {
    return Vue.http.get('/announcements', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createAnnouncement = (payload) => {
    return Vue.http.post('/announcements', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewAnnouncement = (payload) => {
    return Vue.http.get('/announcements/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editAnnouncement = (payload) => {
    return Vue.http.put('/announcements/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};