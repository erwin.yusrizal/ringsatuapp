/* jshint esversion: 6 */

import Vue from 'vue';

export const getBanks = (payload) => {
    return Vue.http.get('/banks', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};