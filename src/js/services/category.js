/* jshint esversion: 6 */

import Vue from 'vue';

export const getCategories = (payload) => {
    return Vue.http.get('/categories', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const getCategory = (payload) => {
    return Vue.http.get('/categories/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};