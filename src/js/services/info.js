/* jshint esversion: 6 */

import Vue from 'vue';

export const getInfos = (payload) => {
    return Vue.http.get('/info', {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const createInfo = (payload) => {
    return Vue.http.post('/info', payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const viewInfo = (payload) => {
    return Vue.http.get('/info/'+payload.id, {params: payload})
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const editInfo = (payload) => {
    return Vue.http.put('/info/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteInfo = (payload) => {
    return Vue.http.delete('/info/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};