/* jshint esversion: 6 */

import Vue from 'vue';
import store from '../store';

export const editMedia = (payload) => {
    return Vue.http.post('/media/'+payload.endpoint+'/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteTmpMedia = (payload) => {
    return Vue.http.delete('/media/tmp/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};

export const deleteMedia = (payload) => {
    return Vue.http.delete('/media/'+payload.endpoint+'/'+payload.id, payload)
        .then((response) => Promise.resolve(response))
        .catch((error) => Promise.reject(error));
};