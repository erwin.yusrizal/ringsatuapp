/* jshint esversion:6 */

import Vue from 'vue';
import store from './store';
import LoginPage from '../pages/auth-login.vue';
import NotFoundPage from '../pages/404.vue';


var routes = [
	{
		path: '/',
		async: function (routeTo, routeFrom, resolve, reject) {
			const router = this;
			const HomePage = () => import('../pages/home.vue');

			HomePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/login/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const LoginPage = () => import('../pages/auth-login.vue');

			LoginPage().then((vc) => {
				resolve({ component: vc.default });
			});			
		}
	},
	{
		path: '/panic/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const PanicPage = () => import('../pages/panic.vue');

			PanicPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/panic/groups/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const PanicGroupPage = () => import('../pages/panic-groups.vue');

			PanicGroupPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/panic/groups/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const PanicGroupCreatePage = () => import('../pages/panic-groups-form.vue');

			PanicGroupCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/panic/groups/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const PanicGroupEditPage = () => import('../pages/panic-groups-form.vue');

			PanicGroupEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/panic/groups/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const PanicGroupViewPage = () => import('../pages/panic-groups-detail.vue');

			PanicGroupViewPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/marketplace/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const MarketplacePage = () => import('../pages/marketplace.vue');

			MarketplacePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/chat/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ChatPage = () => import('../pages/chats.vue');

			ChatPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/chat/sender/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ChatSenderPage = () => import('../pages/chats-sender.vue');

			ChatSenderPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/chat/recipient/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ChatRecipientPage = () => import('../pages/chats-recipient.vue');

			ChatRecipientPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/chat/:id/messages/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ChatMessagesPage = () => import('../pages/chats-messages.vue');

			ChatMessagesPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/marketplace/category/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const MarketplaceListPage = () => import('../pages/marketplace-list.vue');

			MarketplaceListPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/marketplace/product/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const MarketplaceDetailPage = () => import('../pages/marketplace-detail.vue');

			MarketplaceDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/marketplace/product/:id/comments/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const MarketplaceCommentPage = () => import('../pages/marketplace-comments.vue');

			MarketplaceCommentPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/marketplace/store/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const MarketplaceStorePage = () => import('../pages/marketplace-store.vue');

			MarketplaceStorePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountPage = () => import('../pages/account.vue');

			AccountPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/view/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountDetailPage = () => import('../pages/account-detail.vue');

			AccountDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/edit/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountFormPage = () => import('../pages/account-form.vue');

			AccountFormPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/family/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountFamilyPage = () => import('../pages/account-family.vue');

			AccountFamilyPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/store/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountStorePage = () => import('../pages/account-store.vue');

			AccountStorePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/banks/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountBankPage = () => import('../pages/account-banks.vue');

			AccountBankPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/banks/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountBankCreatePage = () => import('../pages/account-banks-form.vue');

			AccountBankCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/account/banks/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AccountBankEditPage = () => import('../pages/account-banks-form.vue');

			AccountBankEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/contacts/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ContactPage = () => import('../pages/contacts.vue');

			ContactPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/cctv/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const CCTVPage = () => import('../pages/cctv.vue');

			CCTVPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/cctv/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const CCTVViewPage = () => import('../pages/cctv-view.vue');

			CCTVViewPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/announcements/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AnnouncementPage = () => import('../pages/announcements.vue');

			AnnouncementPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/announcements/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AnnouncementDetailPage = () => import('../pages/announcements-detail.vue');

			AnnouncementDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/announcements/view/:id/comments/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const AnnouncementCommentPage = () => import('../pages/announcements-comments.vue');

			AnnouncementCommentPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/resident-reports/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ResidentReportPage = () => import('../pages/resident-reports.vue');

			ResidentReportPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/resident-reports/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ResidentReportDetailPage = () => import('../pages/resident-reports-detail.vue');

			ResidentReportDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/resident-reports/view/:id/comments/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ResidentReportCommentPage = () => import('../pages/resident-reports-comments.vue');

			ResidentReportCommentPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/resident-reports/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ResidentReportCreatePage = () => import('../pages/resident-reports-form.vue');

			ResidentReportCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/resident-reports/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ResidentReportEditPage = () => import('../pages/resident-reports-form.vue');

			ResidentReportEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/financial-reports/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FinancialReportPage = () => import('../pages/financial-reports.vue');

			FinancialReportPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/financial-reports/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FinancialReportDetailPage = () => import('../pages/financial-reports-detail.vue');

			FinancialReportDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/financial-reports/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FinancialReportCreatePage = () => import('../pages/financial-reports-form.vue');

			FinancialReportCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/financial-reports/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FinancialReportEditPage = () => import('../pages/financial-reports-form.vue');

			FinancialReportEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/inventory/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InventoryPage = () => import('../pages/inventories.vue');

			InventoryPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/inventory/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InventoryDetailPage = () => import('../pages/inventories-detail.vue');

			InventoryDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/inventory/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InventoryCreatePage = () => import('../pages/inventories-form.vue');

			InventoryCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/inventory/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InventoryEditPage = () => import('../pages/inventories-form.vue');

			InventoryEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/products/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ProductPage = () => import('../pages/products.vue');

			ProductPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/products/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ProductCreatePage = () => import('../pages/products-form.vue');

			ProductCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/products/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const ProductEditPage = () => import('../pages/products-form.vue');

			ProductEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/info/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InfoPage = () => import('../pages/info.vue');

			InfoPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/info/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InfoCreatePage = () => import('../pages/info-form.vue');

			InfoCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/info/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InfoEditPage = () => import('../pages/info-form.vue');

			InfoEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/info/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InfoDetailPage = () => import('../pages/info-detail.vue');

			InfoDetailPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/info/view/:id/comments/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const InfoCommentPage = () => import('../pages/info-comments.vue');

			InfoCommentPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/getintouch/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const GetInTouchPage = () => import('../pages/getintouch.vue');

			GetInTouchPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/freebies/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FreebiesPage = () => import('../pages/freebies.vue');

			FreebiesPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/freebies/create/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FreebiesCreatePage = () => import('../pages/freebies-form.vue');

			FreebiesCreatePage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/freebies/edit/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FreebiesEditPage = () => import('../pages/freebies-form.vue');

			FreebiesEditPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/freebies/view/:id/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const FreebiesViewPage = () => import('../pages/freebies-detail.vue');

			FreebiesViewPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '/notifications/',
		async: function (routeTo, routeFrom, resolve, reject) {

			const router = this;
			const NotificationPage = () => import('../pages/notifications.vue');

			NotificationPage().then((vc) => {
				if(store.getters['auth/isLoggedIn']){
					resolve({ component: vc.default });				
				}else{
					reject();
					router.navigate('/login/');
					return false;	
				}
			});			
		}
	},
	{
		path: '(.*)',
		component: NotFoundPage
	},
];

export default routes;
