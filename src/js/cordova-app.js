/* jshint esversion:6 */

var cordovaApp = {
	f7: null,
	/*
	This method hides splashscreen after 2 seconds
	*/
	handleSplashscreen: function() {
		var f7 = cordovaApp.f7;
		if (!window.navigator.splashscreen || f7.device.electron) return;
		setTimeout(() => {
			window.navigator.splashscreen.hide();
		}, 2000);
	},
	/*
	This method to handle application keep running on background mode
	*/
	handleBackgroundMode: function(){
		cordova.plugins.backgroundMode.setEnabled(true);
		cordova.plugins.backgroundMode.setDefaults({ silent: true });
		cordova.plugins.backgroundMode.on('activate', function() {
			cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
		});
	},
	/*
	This method prevents back button tap to exit from app on android.
	And allows to exit app on backbutton double tap
	*/
	handleAndroidBackButton: function () {
		var f7 = cordovaApp.f7;
		var $ = f7.$;
		document.addEventListener('backbutton', function (e) {
			var mainView = f7.views.main;
			var leftp = f7.panel.left && f7.panel.left.opened;
			var rightp = f7.panel.right && f7.panel.right.opened;
			if ( leftp || rightp ) {
				f7.panel.close();
				return false;
			} else if ($('.modal-in').length > 0) {
				f7.dialog.close();
				return false;
			} else if (f7.views.main.router.url == '/') {
				f7.dialog.confirm('Anda yakin ingin tutup aplikasi?', 'Tutup', function() {
					navigator.app.exitApp();
				},
				function() {
				});
			} else {
				mainView.router.back();
			}
		}, false);
	},
	/*
	This method does the following:
		- provides cross-platform view "shrinking" on keyboard open/close
		- hides keyboard accessory bar for all inputs except where it required
	*/
	handleKeyboard: function () {
		var f7 = cordovaApp.f7;
		if (!window.Keyboard || !window.Keyboard.shrinkView || f7.device.electron) return;
		var $ = f7.$;
		window.Keyboard.shrinkView(false);
		window.Keyboard.disableScrollingInShrinkView(true);
		window.Keyboard.hideFormAccessoryBar(true);
		window.addEventListener('keyboardWillShow', () => {
			f7.input.scrollIntoView(document.activeElement, 0, true, true);
		});
		window.addEventListener('keyboardDidShow', () => {
			f7.input.scrollIntoView(document.activeElement, 0, true, true);
		});
		window.addEventListener('keyboardDidHide', () => {
			if (document.activeElement && $(document.activeElement).parents('.messagebar').length) {
				return;
			}
			window.Keyboard.hideFormAccessoryBar(false);
		});
		window.addEventListener('keyboardHeightWillChange', (event) => {
			var keyboardHeight = event.keyboardHeight;
			if (keyboardHeight > 0) {
				// Keyboard is going to be opened
				document.body.style.height = `calc(100% - ${keyboardHeight}px)`;
				$('html').addClass('device-with-keyboard');
			} else {
				// Keyboard is going to be closed
				document.body.style.height = '';
				$('html').removeClass('device-with-keyboard');
			}

		});
		$(document).on('touchstart', 'input, textarea, select', function (e) {
			var nodeName = e.target.nodeName.toLowerCase();
			var type = e.target.type;
			var showForTypes = ['datetime-local', 'time', 'date', 'datetime'];
			if (nodeName === 'select' || showForTypes.indexOf(type) >= 0) {
				window.Keyboard.hideFormAccessoryBar(false);
			} else {
				window.Keyboard.hideFormAccessoryBar(true);
			}
		}, true);
	},
	handleDeviceId: function(){
		var notificationOpenedCallback = function(jsonData) {
            // console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
        };

        window.plugins.OneSignal
        .startInit("6bc67df0-7c2c-4b78-8719-c17eb0f94766")
        .handleNotificationOpened(notificationOpenedCallback)
        .endInit();

        window.plugins.OneSignal.getPermissionSubscriptionState(function(s){
            window.localStorage.setItem('nid', s.subscriptionStatus.userId);
        });
	},
	handleLocation: function(){
		var f7 = cordovaApp.f7;
		BackgroundGeolocation.configure({
			locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
			desiredAccuracy: 10,
			stationaryRadius: 10,
			distanceFilter: 10,
			debug: false,
			interval: 30000,
			fastestInterval: 10000,
			activitiesInterval: 30000,
			stopOnStillActivity: false,
			maxLocations: 1000,
			startOnBoot: true,
      		stopOnTerminate: true
		});

		BackgroundGeolocation.on('location', function(location) {
			// handle your locations here
			// to perform long running operation on iOS
			// you need to create background task
			f7.methods.addCurrentLocation(location);
			BackgroundGeolocation.startTask(function(taskKey) {
			  	// execute long running task
			  	// eg. ajax post location
				// IMPORTANT: task has to be ended by endTask
			  	BackgroundGeolocation.endTask(taskKey);
			});
		});
		// BackgroundGeolocation.on('error', function(error) {
		// 	console.log('[ERROR] BackgroundGeolocation error:', error.code, error.message);
		// });
		
		// BackgroundGeolocation.on('start', function() {
		// 	console.log('[INFO] BackgroundGeolocation service has been started');
		// });
		
		// BackgroundGeolocation.on('stop', function() {
		// 	console.log('[INFO] BackgroundGeolocation service has been stopped');
		// });

		// BackgroundGeolocation.on('background', function() {
		// 	console.log('[INFO] App is in background');
		// 	// you can also reconfigure service (changes will be applied immediately)
		// 	BackgroundGeolocation.configure({ debug: true });
		// });
		
		// BackgroundGeolocation.on('foreground', function() {
		// 	console.log('[INFO] App is in foreground');
		// 	BackgroundGeolocation.configure({ debug: false });
		// });
		
		BackgroundGeolocation.on('authorization', function(status) {
			if (status !== BackgroundGeolocation.AUTHORIZED) {
			  // we need to set delay or otherwise alert may not be shown
			  setTimeout(function() {
				var showSettings = confirm('App requires location tracking permission. Would you like to open app settings?');
				if (showSetting) {
				  return BackgroundGeolocation.showAppSettings();
				}
			  }, 1000);
			}
		});
		
		BackgroundGeolocation.checkStatus(function(status) {	
			// you don't need to check status before start (this is just the example)
			if (!status.isRunning) {
			  	BackgroundGeolocation.start(); //triggers start on start event
			}
		});

		BackgroundGeolocation.start();
	},
	init: function (f7) {
		// Save f7 instance
		cordovaApp.f7 = f7;

		// handle background mode
		cordovaApp.handleBackgroundMode();

		// Handle Android back button
		cordovaApp.handleAndroidBackButton();

		// Handle Statusbar
		cordovaApp.handleSplashscreen();

		// Handle Keyboard
		cordovaApp.handleKeyboard();

		// Handle Device
		cordovaApp.handleDeviceId();

		// Handle Location
		cordovaApp.handleLocation();
	},
};
export default cordovaApp;
