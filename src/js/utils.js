/* jshint esversion: 6 */

import moment from 'moment';

export function dateFormat(date, format){
    moment.locale('id_id');
    format = format || 'DD/MM/YYYY HH:mm';
    return moment(date).format(format);
}

export function convertAmpersand(text){
    return text.replace(/&amp;/g, '&');
}

export function hoursAgo(date){
    moment.locale('id_id');
    let now = moment(new Date());
    let end = moment(date);
    let duration = moment.duration(now.diff(end));
    let days = duration.asDays();

    if(days >= 1){
        return moment(date).format('DD MMM YYYY');
    }else{
        return moment(date).format('HH:mm');
    }
}

export function capitalize(str){
    return str.replace(/\w\S*/g, function(txt){
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
}

export function truncate(str, len){
    len = len || 100;
    return str.length > len ? str.substring(0, len) + '...': str;
}

export function stripHTML(str){
    let regex = /(<([^>]+)>)/ig;
    return str.replace(regex, "");
}

export function initial(str){
    return Array.prototype.map.call(str.split(" "), function(x,y){ return y < 2 ? x.substring(0,1).toUpperCase():'';}).join('');
}

export function parseError(e){
    let error = {};

    for(const key in e.messages){
        const field  = key.replace('_', ' ');
        error.field = capitalize(field);
        error.messages = e.messages[key].join(', ');
    }

    return error;
}

export function spelledOut(number){

    var readThousand = function (n,dg, snum) {
        var s='';
        var d1=Math.floor(n/100);
        var d2=Math.floor((n-(d1*100))/10);
        var d3=n-(d1*100)-(d2*10);

        if (d1>0) {
            if (d1==1) {
                s=s+'seratus ';
            } else {
                s=s+snum[d1]+' ratus ';
            }
        }

        if (d2>0) {
            if (d2==1) {
                switch(d3) {
                    case 0: s=s+'sepuluh ';
                        break;
                    case 1: s=s+'sebelas ';
                        break;
                    default: s=s+snum[d3]+' belas ';
                }
            } else {
                s=s+snum[d2]+' puluh ';
            }
        }

        if (d3>0) {
            if ((d2>1)||(d2==0)) {
                if ((dg==1)&(d3==1)) {
                    s=s+'se';
                } else {
                    s=s+snum[d3]+' ';
                }
            }
        } 
        return s; 
    };

    var readAll = function (x) {
        var s='';
        var i=0;
        var isfailed=false;

        var snum=new Array('nol','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan');
        var thousandDesc=new Array('','ribu','juta','miliar','triliun');

        if (isNaN(x)) {
            isfailed=true;
            throw new Error('Number is required!');
        } else {
            x=parseFloat(x);
        }

        if (isfailed==false) {
            do {
                var groupNumbers = Math.round(((Math.floor(x)/1000)-Math.floor((Math.floor(x)/1000)))*1000);

                s=readThousand(groupNumbers,i,snum)+thousandDesc[i]+' '+s;
                if (x==0) {
                    s='nol';
                }
                x=Math.floor(Math.floor(x)/1000);
                i++;
            } while (x>0);
                return s.replace(/^\s*|\s(?=\s)|\s*$/g, '');
        } else {
                return 'NaN';
        }
    };
    var ret = readAll(number);
    return ret;
}

export function slugify(text){
    return text.toString().toLowerCase()
        .replace(/\s+/g, '-')
        .replace(/[^\w\-]+/g, '')
        .replace(/\-\-+/g, '-')
        .replace(/^-+/, '')
        .replace(/-+$/, '');
}

export function cardType(cardNumber){
    if(/^4[0-9]{6,}$/.test(cardNumber) == true){
        return 'logo-visa';
    }else if(/^5[1-5][0-9]{5,}|222[1-9][0-9]{3,}|22[3-9][0-9]{4,}|2[3-6][0-9]{5,}|27[01][0-9]{4,}|2720[0-9]{3,}$/.test(cardNumber) == true){
        return 'logo-mastercard';
    }else{
        return 'no-type';
    }
}

export function distance(lat1,lng1,lat2,lng2) {
	var R = 6371;
	var dLat = (lat2-lat1) * Math.PI / 180;
	var dLon = (lng2-lng1) * Math.PI / 180;
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
		Math.sin(dLon/2) * Math.sin(dLon/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;
	if (d>1) return Math.round(d).kformat()+" km";
	else if (d<=1) return Math.round(d*1000).kformat()+" m";
	return d.kformat();
}

export function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}